odoo.define('web.realex_sca', function(require) {
	'use strict';
	
	var website = require('website.website');
	
	$(document).ready(function() {
		console.log("loading validation");
		var validObj = $("#sca_info").validate({
			errorClass: "scaError",
			errorPlacement: function (error, element) {
				var parentElem = $(element.parent("div"));
				var errorElem = $(parentElem).find(".errorSpan")
				if (errorElem){
					error.appendTo($(errorElem));
				} else {
					error.insertAfter(element);
				}
			},
			
			highlight: function (element, errorClass, validClass) {
				var elem = $(element);
				elem.addClass(errorClass);
			},
			
			unhighlight: function (element, errorClass, validClass) {
				var elem = $(element);
				elem.removeClass(errorClass);
			},
		});
		
		$(document).on("change", function() {
			if (!$.isEmptyObject(validObj.submitted)) {
				validObj.form();
			}
		});
		
		$(".hpp_post").on('submit', function(e) {
			var isValid = $(".sca_info").valid();
			if (!isValid) {
				e.preventDefault();
				console.log("form submit blocked");
			}
		});
		
		//populate hpp email for post
		$("#email").on("change", function() {
			if($("#hpp_customer_email")) {
				$("#hpp_customer_email").val($("#email").val());
			}
		});
		
		//populate hpp phone for post
		$("#phone").on("change", function() {
			if($("#hpp_customer_phone")) {
				$("#hpp_customer_phone").val($("#phone").val());
			}
		});
		
		//populate hpp billing street 1 for post
		$("#billing_1").on("change", function() {
			if($("#hpp_billing_st1")) {
				$("#hpp_billing_st1").val($("#billing_1").val());
			}
		});
		
		//populate hpp billing street 2 for post
		$("#billing_2").on("change", function() {
			if($("#hpp_billing_st2")) {
				$("#hpp_billing_st2").val($("#billing_2").val());
			}
		});
		
		//populate hpp billing street 3 for post
		$("#billing_3").on("change", function() {
			if($("#hpp_billing_st3")) {
				$("#hpp_billing_st3").val($("#billing_3").val());
			}
		});
		
		//populate hpp billing city for post
		$("#city").on("change", function() {
			if($("#hpp_billing_city")) {
				$("#hpp_billing_city").val($("#city").val());
			}
		});
		
		//populate hpp billing postcode for post
		$("#postcode").on("change", function() {
			if($("#hpp_billing_postcode")) {
				$("#hpp_billing_postcode").val($("#postcode").val());
			}
		});
		
		//populate hpp billing country for post
		$("#country").on("change", function() {
			if($("#hpp_billing_country")) {
				$("#hpp_billing_country").val($("#country").val());
			}
		});
		
		//populate hpp billing state for post
		$("#state").on("change", function() {
			if($("#hpp_billing_state")) {
				$("#hpp_billing_state").val($("#state").val());
			}
		});
		
		
		
	});
});