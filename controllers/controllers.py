# -*- coding: utf-8 -*-
from openerp import http
import logging
import datetime
import hashlib
import json
import socket
import requests
import urllib
from decimal import *
from werkzeug.exceptions import HTTPException, NotFound
from openerp.http import Response 
from openerp.http import local_redirect, request
import re

_logger = logging.getLogger(__name__)


class TranslationieRealexPaymentIntegration(http.Controller):
	#Redirect to HPP and pay full amount
	@http.route('/pay/', auth='public', website=True)
	def pay(self, docid, **kw):
		document = http.request.env['sale.order'].sudo().search([('x_order_uid','like',docid)])
		if document and not document.x_fully_paid:
			hpp_info = document.get_hpp_values(secure3d=True)
			realex_settings = http.request.env['custom_payments.realex'].sudo().browse(1)
			
			return http.request.render('translationie_realex_payment_integration.payment_page', {
				'document': document, 'hpp_info':hpp_info, 'realex':realex_settings
				})
		else:
			return http.request.render('translationie_realex_payment_integration.not_found',{})

	#Redirect to HPP and pay half
	@http.route('/deposit/', auth='public', website=True)
	def deposit(self, docid, **kw):
		# try:
		document = http.request.env['sale.order'].sudo().search([('x_order_uid','like',docid)])
		if document and document.x_deposit_allowed:
			hpp_info = document.get_hpp_values(deposit=True, secure3d=True)
			realex_settings = http.request.env['custom_payments.realex'].sudo().browse(1)
			
			
			return http.request.render('translationie_realex_payment_integration.downpayment_page', {
				'document': document, 'hpp_info':hpp_info, 'realex':realex_settings
				})
		else:
			return http.request.render('translationie_realex_payment_integration.not_found',{})
	
	#
	@http.route('/payment_success', auth='public', methods=['GET'], website=True, csrf=False)
	def success(self, **kw):
		_logger.debug("payment successful")
		realex_settings = http.request.env['custom_payments.realex'].sudo().browse(1)
		data = http.request.httprequest.args
		docid = data['docid']
		document = http.request.env['sale.order'].sudo().search([('x_order_uid','like',docid)])
		_logger.debug("data is: " + str(data))
		return http.request.render('translationie_realex_payment_integration.confirmation_page',{
			'document': document, 'receipt_info': data, 'realex': realex_settings
		})
	
	@http.route('/payment_failure', auth='public', methods=['GET'], website=True, csrf=False)
	def failure(self, **kw):
		_logger.debug("payment failure")
		data = http.request.httprequest.args
		message_data = dict(http.request.httprequest.args)
		docid = data['docid']
		document = http.request.env['sale.order'].sudo().search([('x_order_uid','like',docid)])
		_logger.debug("full data is: " + str(data))
		_logger.debug("data message is: " + str(data['failure_message']))
		message = message_data['failure_message']
		_logger.debug("message is: " + str(message))
		return http.request.render('translationie_realex_payment_integration.failure_page',{
			'document': document, 'failure_message': [line for line in message],
		})
	
	#Redirects to either "success" or "failure" controller methods based on RESULT HPP returns
	@http.route('/payment_process', auth='public', type='http', methods=['POST','GET'], website=True, csrf=False)
	def payment_process(self, **kwargs):
		form = http.request.httprequest.form
		realex = http.request.env['custom_payments.realex'].sudo().browse(1)
		_logger.debug("form data is: " + str(form))
		#NEED to set param "web.base.url" in Odoo ir.config_parameter
		try:
			base_url = http.request.env['ir.config_parameter'].sudo().get_param('web.base.url')
			base_url_ssl = base_url.replace('http', 'https')
			reTransactionFail = r"1\d{2}"
			
			if form['RESULT'] == '00':
				url = base_url_ssl + '/payment_success'
				data = form
				data = realex.handle_response_success(data)
				_logger.debug("PAYMENT SUCCESSFUL, REDIRECTING TO: " + str(url))
				return  local_redirect(url, dict(data), True, code=200)
				
			else:
				url = base_url_ssl + '/payment_failure'
				data = realex.handle_response_failure(form)
				_logger.debug("PAYMENT FAILED, REDIRECTING TO: " + str(url))
				return local_redirect(url, dict(data), True, code=200)
		except Exception:
			_logger.error("error processing payment response", exc_info=True)
	
	@http.route('/view_tasks', auth='public', type='http', methods=['GET'], csrf=False)
	def view_tasks(self, docid, **kw):
		document = http.request.env['sale.order'].sudo().search([('id','=',docid)])
		result = document.action_view_task()
		
		imd = http.request.env['ir.model.data']
		iuv = http.request.env['ir.ui.view']
		list_view_id = imd.xmlid_to_res_id('project.view_task_tree2')
		view = iuv.browse(list_view_id)
		_logger.debug("view is: " + str(view))
		return view
		

	
